## Heimdall
### Discord bot for SnowGem https://snowgem.org/

v.1.4 - deployed 16.02.2020
- implemented repo feeder into [Snowgem Development Progress](https://github.com/Snowgem/SnowgemDevelopmentProgress)

v.1.0 - deployed 23.01.2019
- initial version
- implemented these commands:
  help, netinfo, mninfo, hpow, xsgusd, mnrewards, roadmap, coininfo, about, members (CoreTeam only)


  NOTE: need to create a empty list file `[]` with name dev-diary.json in main folder
